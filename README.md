Para poder buscar películas con la API de The Movie DB, se realizó lo siguiente:

Primero se creó la función buscarPeliculas, que recibe palabras clave de los títulos de las películas y hace la solicitud a la API de The Movie DB para la búsqueda de películas.

Se generó una APIkey realizando todo el procedimiento de solicitud en el sitio de The Movie DB para que fuera posible que la API diera respuestaa las búsquedas.

En la parte de la ajax, se especifica la url con la APIKey ya incluida,mas la palabra clave para la búsqueda. 

Cuando la función buscarPeliculas se completa, se llama a otra función, mostrarResultados, que trae los resultados de la respuesta de la API y los muestra en la página.

Al mostrar los resultados, se aplica la función "empty" para no acumular los listados al hacer una nueva búsqueda. 

Después, con el bucle for, se agragan los títulos de las películas a la lista de resultados de la búsqueda.
